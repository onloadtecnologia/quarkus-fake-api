package org.acme;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.acme.models.Cliente;

@Path("/cliente")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ClienteController {

    private  List<Cliente> banco = new ArrayList<Cliente>();

    @GET
    public Response findAll() {
        return  Response.ok(this.banco).build();
    }

    @GET
    @Path("/{id}")
    public Response findById(Long id) {
        var  cliente = this.banco.stream().filter(cl -> cl.getId() == id);
        return  Response.ok(cliente).build();
    }

    @POST
    @Transactional
    public Response save(Cliente cliente){
        cliente.setId(this.banco.size() + 1);
        this.banco.add(cliente);
        return Response.created(URI.create("/cliente")).build();
    }

    @PUT
    @Path("/{id}")
    @Transactional
    public Response update(Long id,Cliente cliente){
        this.banco.forEach(cl -> {
            if(cl.getId() == id){
                cl.setId(cl.getId());
                cl.setData(cl.getData());
                cl.setNome(cliente.getNome());
                cl.setTelefone(cliente.getTelefone());
                cl.setEmail(cliente.getEmail());
                cl.setCep(cliente.getCep());
            }
        });
        return Response.accepted(this.banco).build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(Long id) {
        var  result = this.banco.removeIf(cl -> cl.getId() == id);
        return  Response.ok(result).build();
    }
}